from django.db import models

# Create your models here. 

class BaseModel(models.Model):
    created_by = models.ForeignKey("auth.User", blank=True, null=True, related_name="+")
    updated_by = models.ForeignKey("auth.User", blank=True, null=True, related_name="+")
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)
    status = models.BooleanField(default=True)

    class Meta:
        abstract = True
