from django.core.mail import EmailMessage
from django.core.mail.message import EmailMultiAlternatives
from django.http import HttpResponse

def send_signup_mail(name, to):
    subject, from_email = 'Salereporter: User registration', "Salereporter <noreply@salereporter.com>"
    text_content = """Hello %s,
Thank you for registering with Salereporter. We will review the information you submitted and get back to you shortly.
Best Regards,
Salereporter Team
""" % (name)
    #html_content = '<p>This is an <strong>important</strong> message.</p>'
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to, 'hi@salereporter.com'])
    #msg.attach_alternative(html_content, "text/html")
    msg.send()

def send_activation_mail(name, to):
    subject, from_email = 'Salereporter: User Activated', "Salereporter <noreply@salereporter.com>"
    text_content = """Hello %s,
Your business has been activated. Please log in to your account by clicking on the following link:
http://salereporter.com/business/login/
If you have any issues, please check our support site www.salereporter.com/support
Welcome Aboard!
Best Wishes,
Salereporter Team
""" % (name)
    html_content = 'Hello '+name+'<br>Your business has been activated. Please log in to your account by clicking on the following link: <br>http://salereporter.com/business/login/ <br> If you have any issues, please check our support site www.salereporter.com/support<br>Welcome Aboard!<br>Best Wishes<br>Salereporter Team'
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to, 'hi@salereporter.com'])
    msg.attach_alternative(html_content, "text/html")
    msg.send()