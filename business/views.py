from django.shortcuts import render, render_to_response
from form import OfferForm, ForgotPasswordForm, NewPasswordForm
from django.core.context_processors import csrf
from django.contrib.auth.models import User
from models import Business, Offer, Category, Follower
from django.http.response import HttpResponseRedirect
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout, hashers
from django.template import RequestContext
from django.contrib import auth
import ast
from django.views.decorators.csrf import csrf_exempt
import json
import math
import requests
from datetime import datetime
from django.contrib import messages
from base import email, reset_password
from django.views.generic.base import View, TemplateView
from django.views.generic.edit import FormView
from django.core.urlresolvers import reverse

def distance_on_unit_sphere(lat1, long1, lat2, long2):
 
    # Convert latitude and longitude to 
    # spherical coordinates in radians.
    degrees_to_radians = math.pi/180.0
         
    # phi = 90 - latitude
    phi1 = (90.0 - lat1)*degrees_to_radians
    phi2 = (90.0 - lat2)*degrees_to_radians
         
    # theta = longitude
    theta1 = long1*degrees_to_radians
    theta2 = long2*degrees_to_radians
         
    # Compute spherical distance from spherical coordinates.
         
    # For two locations in spherical coordinates 
    # (1, theta, phi) and (1, theta', phi')
    # cosine( arc length ) = 
    #    sin phi sin phi' cos(theta-theta') + cos phi cos phi'
    # distance = rho * arc length
     
    cos = (math.sin(phi1)*math.sin(phi2)*math.cos(theta1 - theta2) + 
           math.cos(phi1)*math.cos(phi2))
    arc = math.acos( cos )
 
    # Remember to multiply arc by the radius of the earth 
    # in your favorite set of units to get length.
    return float(arc)*6378.1


# Create your views here.

def register(request):
	arg = {}
	arg.update(csrf(request))
	if request.method == 'POST':
		form_id = business_type = request.POST['form_id']
		if form_id == "1":
			request.session['business_type'] = request.POST['option']
			return render_to_response('business/b_company.html', arg)
		elif form_id == "2":
			request.session['first_name'] = request.POST['firstName']
			request.session['last_name'] = request.POST['lastName']
			request.session['title'] = request.POST['title']
			request.session['phone_no'] = request.POST['phone']
			request.session['business_email'] = request.POST['businessEmail']
			request.session['password'] = request.POST['password']
			if User.objects.filter(email=request.POST['businessEmail']).exists():
				arg['error'] = "Email already exists."
				arg['first_name'] = request.POST['firstName']
				arg['last_name'] = request.POST['lastName']
				arg['phone_no'] = request.POST['phone']
				arg['title'] = request.POST['title']
				arg['business_email'] = request.POST['businessEmail']
				return render_to_response('business/b_company.html', arg)
			return render_to_response('business/b_profile.html', arg)
		elif form_id == "3":
			first_name = request.session['first_name']
			last_name = request.session['last_name']
			title = request.session['title']
			phone_no = request.session['phone_no']
			business_email = request.session['business_email']
			password = request.session['password']
			lat = request.POST['lat']
			longi = request.POST['longi']
			business_name = request.POST['nameBusiness']
			street = request.POST['street']
			suite = request.POST['suite']
			city = request.POST['city']
			state = request.POST['state']
			zip_code = request.POST['zip']
			cat = Category.objects.get(id= request.session['business_type'])
			business_type = cat
			business_phone = request.POST['phoneNo']
			website = request.POST['website']
			# create user
			user = User.objects.create_user(business_email, business_email, password)
			user.last_name = last_name
			user.first_name = first_name
			user.is_active = False
			user.save()
			business = Business()
			business.first_name = first_name
			business.last_name = last_name
			business.title = title
			business.phone_no = phone_no
			business.business_email = business_email
			business.password = password
			business.business_name = business_name
			business.street = street
			business.suite = suite
			business.city = city
			business.state = state
			business.business_phone = business_phone
			business.website = website
			business.user = user
			business.business_type = business_type
			business.zip_code = zip_code
			business.lat = lat
			business.longi = longi
			business.save()
			try:
				email.send_signup_mail(first_name, business_email)
			except:
				pass
			messages.success(request, 'Thank you for registering. We appreciate your interest. Someone from our team will get back to you soon!')
			return HttpResponseRedirect('/business/login')
		return HttpResponse(business_type)
	return render_to_response('business/b_register.html', arg)

def log_in(request):
	""" This Function is used for user login """
	arg = {}
	arg.update(csrf(request))
	if request.user.is_authenticated():
		return HttpResponseRedirect('/business/home/')
	if request.POST:
		# redirect_to = request.REQUEST.get('next', '')
	    #username = password = ''
		username = request.POST['username']
		password = request.POST['password']
		user = authenticate(username=username, password=password)
		if user is None:
			arg['error'] = "Invalid username or Password"
		elif user.is_active == False:
			arg['error'] = "User is not activated"
		else:
			login(request, user)
			return HttpResponseRedirect('/business/home')
	return render_to_response('business/login.html', arg, context_instance=RequestContext(request))
@login_required
def home(request):
	arg = {}
	arg['offers'] = Offer.objects.filter(user=request.user)
	return render_to_response('business/b_dashbrd.html', arg, context_instance=RequestContext(request))
@login_required
@csrf_exempt
def make_offer(request):
	arg = {}
	data = {}
	arg['form'] = OfferForm()
	arg['btn'] = 'Add'
	arg['cancel_url'] = '/business/home'
	arg.update(csrf(request))
	if request.method == 'POST':
		form = OfferForm(request.POST)
		arg['form'] = OfferForm(request.POST)
		if arg['form'].is_valid():
			form = OfferForm(request.POST)
			business = Business.objects.get(user=request.user)
			lat1 = float(business.lat)
			long1 = float(business.longi)
			gsm_ids = []
			followers = Follower.objects.all()
			for follower in followers:
				if follower.lat != None and follower.longi != None:
					lat2 = float(follower.lat)
					lobg2 = float(follower.longi)
					#calculate the distance and add to list if distance is less than 50k
					distance = distance_on_unit_sphere(lat1, long1, lat2, lobg2)
					business_cat = business.business_type
					follower_cat = follower.pref_cat.all()
					if distance < 50.0 and business_cat in follower_cat:
						gsm_ids.append(follower.gcm_id)
				else:
					#add to list if location data not found
					gsm_ids.append(follower.gcm_id)
			message = request.POST['offer']
			starts = request.POST['starts']
			ends = request.POST['ends']
			date =datetime.strptime(ends, "%Y/%m/%d %I:%M %p")
			business_address = business.business_name+", "+business.street+", "+business.city+", "+ business.state
			
			item = {"data": {"message": ""+message+ " till "+date.strftime("%I:%M %p on %B %d, %Y")+" at "+business_address ,}}
			item['registration_ids'] = gsm_ids
			item['content_available'] = True

			noti = {}
			noti['content_available'] = True
			noti['notification'] = {}
			noti['notification']['body'] = message+ " till "+date.strftime("%I:%M %p on %B %d, %Y")+" at "+business_address
			noti['data'] = {}
			noti['data']['message'] = message+ " till "+date.strftime("%I:%M %p on %B %d, %Y")+" at "+business_address
			noti['data']['title'] = "New Offer"
			noti['notification']['sound'] = 'default'
			noti['registration_ids'] = gsm_ids
            data['tokens'] = gsm_ids
			noti['priority'] = 'high'
            # main API AIzaSyBgjSTNM-AWHovf25d6mrmX7No23yycWqk
			#return HttpResponse(json.dumps(item), content_type="application/json")
			resp = requests.post("https://gcm-http.googleapis.com/gcm/send", data=json.dumps(noti),headers={"Content-Type":"application/json",'Authorization': 'key=%s' % "AIzaSyB2R4E6G6cR9tD3jdpEVhzaA59AGBWZI1k"})
			obj = arg['form'].save(commit=False)
			obj.user = request.user
			obj.save()
           
			data['status'] ="success"
		else:
			data['status'] ="falure"
		return HttpResponse(json.dumps(data), content_type="application/json")
	return render_to_response('business/b_offer.html', arg, context_instance=RequestContext(request))
def log_out(request):
	return render_to_response('business/b_offer.html', arg, context_instance=RequestContext(request))
def log_out(request):
	auth.logout(request)
	return HttpResponseRedirect("/business/login")




@csrf_exempt
def register_follower(request):
	arg = {}
	if request.POST:
		email = request.POST['email']
		gcm_id = request.POST['gcm_id']
		lat = request.POST['lat']
		longi = request.POST['longi']
		device_id = request.POST['device_id']
		preff = request.POST['preff']
		if device_id != "":
			try:
			    follower = Follower.objects.get(device_id=device_id)
			except Follower.DoesNotExist:
			    follower = Follower(device_id=device_id)
			    follower.save()
			if email != "":
				follower.email = email
			if gcm_id != "":
				follower.gcm_id = gcm_id
			if lat != "":
				follower.lat = lat
			if longi != "":
				follower.longi = longi
			if preff != "":
				preff_list = json.loads(preff)
				for cat_id in preff_list:
					try:
						cat = Category.objects.get(id=cat_id)
						follower.pref_cat.add(cat)
					except:
						pass
			follower.save()
			arg['status'] = "success"
		else:
			arg['status'] = "failure"
	return HttpResponse(json.dumps(arg), content_type="application/json")
# def follow_resturent(request):

class PasswordForgotView(FormView):
    template_name = 'business/b_forgot_password.html'
    form_class = ForgotPasswordForm

    def form_valid(self, form):
        user = User.objects.get(email=form.cleaned_data['email'])
        self.user = user
        link = reset_password.generate_unique_reset_password_link(user)
        form.send_reset_link(link)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
    	return reverse('login')

class NewPasswordView(FormView):
    form_class = NewPasswordForm
    template_name = 'business/b_new_password.html'


    def dispatch(self, request, *args, **kwargs):
        email_hash = kwargs.pop('email_hash')
        email = reset_password.unsign_email_hash(email_hash)
        self.user = User.objects.get(email=email)
        return super(NewPasswordView, self).dispatch(request, *args, **kwargs)

    def get_success_url(self):
        return HttpResponseRedirect("/business/login")


    def form_valid(self, form):
        self.user.set_password(form.cleaned_data['password'])
        self.user.save()
        return self.get_success_url()