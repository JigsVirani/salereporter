from django import forms
from models import Offer
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.conf import settings

class OfferForm(forms.ModelForm):
    offer = forms.CharField(label='Offer Details', widget=forms.Textarea(attrs={'class' : 'form-control ', 'placeholder': 'What are you offering? Example:  Regular starbucks coffee for $1 (Limit 200 characters)', 'required': ''}))
    starts = forms.CharField(label='Offer Starts', widget=forms.TextInput(attrs={'class' : 'form-control datetime', 'required' : 'true', 'placeholder' : 'Start'}))
    ends = forms.CharField(label='Offer Ends', widget=forms.TextInput(attrs={'class' : 'form-control datetime', 'required': 'true', 'placeholder' : 'Ends'}))
    class Meta:
        model = Offer
        fields = ('offer', 'starts', 'ends')

class ForgotPasswordForm(forms.Form):
    email = forms.EmailField(required=True, widget=forms.EmailInput(attrs={'class': 'form-control',
                                                                           'placeholder': 'Email Address'}))

    def clean_email(self):
        email = self.cleaned_data['email']
        try:
            #import ipdb; ipdb.set_trace()
            User.objects.get(email=email)
        except User.DoesNotExist:
            raise forms.ValidationError("Email address does not exist.")
        return email

    def send_reset_link(self, link):
        title = "Password reset link"
        message = "Follow this link to reset your password: {0}".format(link)
        try:
            send_mail(title, message, from_email=settings.CONTACT_EMAIL, recipient_list=[self.cleaned_data['email']])
        except:
            pass

class NewPasswordForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control',
                                                                 'placeholder': 'New password'}))
    password_confirm = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control',
                                                                         'placeholder': 'Password confirm'}))

    def clean_password_confirm(self):
        password = self.cleaned_data['password']
        password_confirm = self.cleaned_data['password_confirm']

        if not password_confirm:
            raise forms.ValidationError('You must confirm your new password.')

        if password and password_confirm and password != password_confirm:
            raise forms.ValidationError('Passwords must match.')
