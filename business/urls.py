from django.conf.urls import url
from views import PasswordForgotView, NewPasswordView

urlpatterns = [
    #url(r'^admin/', admin.site.urls),
    url(r'^register/', 'business.views.register'),
    url(r'^register_floower/', 'business.views.register_follower'),
    url(r'^home/$', 'business.views.home'),
    url(r'^make_offer/', 'business.views.make_offer'),
    url(r'^login/', 'business.views.log_in', name='login'),
    url(r'^logout/', 'business.views.log_out'),
    url(r'^reset-password/$', PasswordForgotView.as_view(), name='reset-password'),
    url(r'^reset-password-form/(?P<email_hash>\S+)/$', NewPasswordView.as_view(), name='reset-password-form'),
    url(r'^$', 'landingpage.views.load_landing'),

]
