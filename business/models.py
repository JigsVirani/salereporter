from __future__ import unicode_literals

from django.db import models
from base.models import BaseModel
from django.contrib.auth.models import User
from django.dispatch import receiver
from base import email

# Create your models here.
#business Type defination
# 1 = Restaurant 
# 2 = Bars 
# 3 = Grocery Stores 
# 4 = Department Stores 

@receiver(models.signals.pre_save)
def _user_activated(sender, instance, **kwargs):
    if sender == User:
        if instance.pk is not None:
        	user = User.objects.get(id=instance.pk)
        	if instance.pk is not None:
        		if user.is_active == False and instance.is_active == True:
        			try:
        				email.send_activation_mail(user.first_name, user.email)
        			except:
        				pass
        	




class Category(BaseModel):
	name = models.CharField(max_length=255)

class Business(BaseModel):
	first_name =  models.CharField(max_length=255)
	last_name =  models.CharField(max_length=255)
	title = models.CharField(max_length=255)
	phone_no = models.CharField(max_length=255)
	business_email = models.CharField(max_length=255)
	password = models.CharField(max_length=255)
	business_name = models.CharField(max_length=255)
	street = models.TextField()
	suite = models.CharField(max_length=255)
	city = models.CharField(max_length=255)
	state = models.CharField(max_length=255)
	business_phone = models.CharField(max_length=255)
	website = models.CharField(max_length=255)
	lat = models.CharField(max_length=255, default=None)
	longi = models.CharField(max_length=255, default=None)
	user = models.ForeignKey(User, default=None)
	business_type =  models.ForeignKey(Category, default=None)
	zip_code = models.CharField(max_length=255, default=None)
	def __unicode__(self):
		return self.first_name + " "+ self.last_name


class Follower(BaseModel):
	device_id = models.CharField(max_length=255, null=True)
	email = models.CharField(max_length=255, null=True)
	gcm_id = models.CharField(max_length=255, null=True)
	lat = models.CharField(max_length=255, null=True)
	longi = models.CharField(max_length=255, null=True)
	following = models.ManyToManyField(Business, default=None)
	pref_cat = models.ManyToManyField(Category, default=None)

class Offer(BaseModel):
	offer = models.CharField(blank=False,max_length=255)
	starts = models.CharField(blank=True, null=True, max_length=50)
	ends = models.CharField(blank=True, null=True, max_length=50)
	user = models.ForeignKey(User, default=None)

