from django.shortcuts import render, render_to_response

# Create your views here.

def load_landing(request):
	arg = {}
	return render_to_response('landingpage/index.html', arg)
